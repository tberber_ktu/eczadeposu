QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
CONFIG += sdk_no_version_check

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    UI/ListeFormlari/eczilaclistesi.cpp \
    UI/ListeFormlari/ecztedarikcilistewidget.cpp \
    UI/VeriFormlari/Widgetlar/tedarikciduzenleme.cpp \
    UI/VeriFormlari/ecztedarikciformu.cpp \
    UI/VeriFormlari/eczyeniilactanimlamaformu.cpp \
    UI/eczalisfaturasiformu.cpp \
    Veri/VeriSiniflari/eczalisfaturasi.cpp \
    Veri/VeriSiniflari/eczeczanebilgileri.cpp \
    Veri/VeriSiniflari/eczilacalisbilgileri.cpp \
    Veri/VeriSiniflari/eczilacbilgileri.cpp \
    Veri/VeriSiniflari/eczilacsatisfaturasi.cpp \
    Veri/VeriSiniflari/eczsatisbilgileri.cpp \
    Veri/VeriSiniflari/ecztedarikci.cpp \
    Veri/VeriSiniflari/eczuretilenilac.cpp \
    Veri/VeriYoneticileri/eczalisfaturasiyoneticisi.cpp \
    Veri/VeriYoneticileri/eczeczanebilgileriyoneticisi.cpp \
    Veri/VeriYoneticileri/eczilacalisbilgileriyoneticisi.cpp \
    Veri/VeriYoneticileri/eczilacbilgisiyoneticisi.cpp \
    Veri/VeriYoneticileri/ecztedarikciyonetici.cpp \
    Veri/eczgenelveriyoneticisi.cpp \
    main.cpp \
    eczanapencere.cpp

HEADERS += \
    UI/ListeFormlari/eczilaclistesi.h \
    UI/ListeFormlari/ecztedarikcilistewidget.h \
    UI/VeriFormlari/Widgetlar/tedarikciduzenleme.h \
    UI/VeriFormlari/ecztedarikciformu.h \
    UI/VeriFormlari/eczyeniilactanimlamaformu.h \
    UI/eczalisfaturasiformu.h \
    Veri/VeriSiniflari/eczalisfaturasi.h \
    Veri/VeriSiniflari/eczeczanebilgileri.h \
    Veri/VeriSiniflari/eczilacalisbilgileri.h \
    Veri/VeriSiniflari/eczilacbilgileri.h \
    Veri/VeriSiniflari/eczilacsatisfaturasi.h \
    Veri/VeriSiniflari/eczsatisbilgileri.h \
    Veri/VeriSiniflari/ecztedarikci.h \
    Veri/VeriSiniflari/eczuretilenilac.h \
    Veri/VeriYoneticileri/eczalisfaturasiyoneticisi.h \
    Veri/VeriYoneticileri/eczeczanebilgileriyoneticisi.h \
    Veri/VeriYoneticileri/eczilacalisbilgileriyoneticisi.h \
    Veri/VeriYoneticileri/eczilacbilgisiyoneticisi.h \
    Veri/VeriYoneticileri/ecztedarikciyonetici.h \
    Veri/VeriYoneticileri/temel_veri_yoneticisi.h \
    Veri/eczgenelveriyoneticisi.h \
    Veri/tanimlar.h \
    eczanapencere.h

FORMS += \
    UI/ListeFormlari/eczilaclistesi.ui \
    UI/ListeFormlari/ecztedarikcilistewidget.ui \
    UI/VeriFormlari/Widgetlar/tedarikciduzenleme.ui \
    UI/VeriFormlari/ecztedarikciformu.ui \
    UI/VeriFormlari/eczyeniilactanimlamaformu.ui \
    UI/eczalisfaturasiformu.ui \
    eczanapencere.ui

TRANSLATIONS += \
    EczeDeposu_en_US.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
