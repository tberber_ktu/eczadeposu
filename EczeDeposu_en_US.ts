<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>ECZAnaPencere</name>
    <message>
        <location filename="eczanapencere.ui" line="14"/>
        <source>ECZAnaPencere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="eczanapencere.ui" line="28"/>
        <source>İlaç Bilgileri</source>
        <translation>Medicine Information</translation>
    </message>
    <message>
        <location filename="eczanapencere.ui" line="32"/>
        <source>Üretilen İlaçlar</source>
        <translation>Produced Medicine</translation>
    </message>
    <message>
        <location filename="eczanapencere.ui" line="43"/>
        <source>Tedarikçi Tanımları</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="eczanapencere.ui" line="50"/>
        <source>Fatura</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="eczanapencere.ui" line="54"/>
        <source>Alış Faturaları</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="eczanapencere.ui" line="61"/>
        <source>Satıs Faturaları</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="eczanapencere.ui" line="71"/>
        <source>Eczane Tanımları</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="eczanapencere.ui" line="84"/>
        <source>toolBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="eczanapencere.ui" line="98"/>
        <source>toolBar_2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="eczanapencere.ui" line="111"/>
        <source>toolBar_3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="eczanapencere.ui" line="124"/>
        <source>toolBar_4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="eczanapencere.ui" line="137"/>
        <source>toolBar_5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="eczanapencere.ui" line="150"/>
        <source>Yeni İlaç Tanımlama</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="eczanapencere.ui" line="155"/>
        <source>Mevcut İlaçlar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="eczanapencere.ui" line="160"/>
        <source>Toplu İlaç Üretme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="eczanapencere.ui" line="165"/>
        <source>Yeni Tedarikçi Tanımla</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="eczanapencere.ui" line="170"/>
        <source>Tedarikçiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="eczanapencere.ui" line="175"/>
        <location filename="eczanapencere.ui" line="185"/>
        <source>Yeni Fatura</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="eczanapencere.ui" line="180"/>
        <location filename="eczanapencere.ui" line="190"/>
        <source>Faturalar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="eczanapencere.ui" line="195"/>
        <source>Yeni Eczane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="eczanapencere.ui" line="200"/>
        <source>Eczaneler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="eczanapencere.cpp" line="63"/>
        <source>Yeni İlaç Ekleme</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ECZIlacListesi</name>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.ui" line="20"/>
        <source>Arama</source>
        <translation>Filter List</translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.ui" line="34"/>
        <location filename="UI/ListeFormlari/eczilaclistesi.ui" line="90"/>
        <source>Değer</source>
        <translation>Value</translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.ui" line="46"/>
        <location filename="UI/ListeFormlari/eczilaclistesi.ui" line="102"/>
        <source>Filtre Türü</source>
        <translation>Filter Type</translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.ui" line="52"/>
        <location filename="UI/ListeFormlari/eczilaclistesi.ui" line="108"/>
        <source>İle Başlayan</source>
        <translation>Starts With</translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.ui" line="59"/>
        <location filename="UI/ListeFormlari/eczilaclistesi.ui" line="115"/>
        <source>İle Biten</source>
        <translation>Ends With</translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.ui" line="66"/>
        <location filename="UI/ListeFormlari/eczilaclistesi.ui" line="122"/>
        <source>İçeren</source>
        <translation>Contains</translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.ui" line="82"/>
        <source>Etken Madde</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.ui" line="140"/>
        <source>Ara</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.ui" line="147"/>
        <source>Yazarken Ara</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.ui" line="175"/>
        <source>Kapat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.cpp" line="44"/>
        <source>İlaç ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.ui" line="26"/>
        <location filename="UI/ListeFormlari/eczilaclistesi.cpp" line="44"/>
        <source>İlaç Adı</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.cpp" line="44"/>
        <source>İlaç Türü</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.cpp" line="44"/>
        <source>Yan Etkileri</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.cpp" line="45"/>
        <source>Etken Maddesi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.cpp" line="45"/>
        <source>Etken Madde Miktarı</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.cpp" line="45"/>
        <source>İlaç Sil</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.cpp" line="46"/>
        <source>İlaç Düzelt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.cpp" line="52"/>
        <location filename="UI/ListeFormlari/eczilaclistesi.cpp" line="90"/>
        <source>%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.cpp" line="94"/>
        <source>İlacı Sil</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.cpp" line="102"/>
        <source>Silme Onayı</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.cpp" line="103"/>
        <source>%1 isimli ilacı simek istediğinize emin misiniz?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.cpp" line="107"/>
        <source>Kayıt Silindi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.cpp" line="108"/>
        <source>Kayıt Silme işlemi tamamlandı!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.cpp" line="114"/>
        <source>İlacı Düzelt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/ListeFormlari/eczilaclistesi.cpp" line="122"/>
        <source>%1 İlacını Düzenle</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ECZYeniIlacTanimlamaFormu</name>
    <message>
        <location filename="UI/VeriFormlari/eczyeniilactanimlamaformu.ui" line="26"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/VeriFormlari/eczyeniilactanimlamaformu.ui" line="34"/>
        <source>İlaç &amp;Adı</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/VeriFormlari/eczyeniilactanimlamaformu.ui" line="47"/>
        <source>İlaç Türü</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/VeriFormlari/eczyeniilactanimlamaformu.ui" line="64"/>
        <source>Tablet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/VeriFormlari/eczyeniilactanimlamaformu.ui" line="71"/>
        <source>Şurup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/VeriFormlari/eczyeniilactanimlamaformu.ui" line="78"/>
        <source>Aşı</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/VeriFormlari/eczyeniilactanimlamaformu.ui" line="89"/>
        <source>Efervesan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/VeriFormlari/eczyeniilactanimlamaformu.ui" line="96"/>
        <source>Krem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/VeriFormlari/eczyeniilactanimlamaformu.ui" line="103"/>
        <source>Fitil</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/VeriFormlari/eczyeniilactanimlamaformu.ui" line="115"/>
        <source>Yan Etkiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/VeriFormlari/eczyeniilactanimlamaformu.ui" line="128"/>
        <source>Etken Maddeler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/VeriFormlari/eczyeniilactanimlamaformu.ui" line="141"/>
        <source>Etken Madde Miktarı</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/VeriFormlari/eczyeniilactanimlamaformu.ui" line="151"/>
        <source> mg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/VeriFormlari/eczyeniilactanimlamaformu.ui" line="191"/>
        <source>Kaydet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI/VeriFormlari/eczyeniilactanimlamaformu.ui" line="198"/>
        <source>İptal</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="Veri/VeriYoneticileri/temel_veri_yoneticisi.h" line="67"/>
        <source>Aranılan veri bulunamadı! Silme işlemi iptal edildi!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Veri/VeriYoneticileri/temel_veri_yoneticisi.h" line="84"/>
        <location filename="Veri/VeriYoneticileri/temel_veri_yoneticisi.h" line="116"/>
        <source>Aranılan veri bulunamadı!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
