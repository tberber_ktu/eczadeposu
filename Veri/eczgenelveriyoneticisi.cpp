#include "eczgenelveriyoneticisi.h"

ECZGenelVeriYoneticisi::ECZGenelVeriYoneticisi(QObject *parent) : QObject(parent)
{
    
}

ECZGenelVeriYoneticisi &ECZGenelVeriYoneticisi::sec()
{
    // Tekil Tasarım Şablonu Adım 3: static nesne tanımlanır
    static ECZGenelVeriYoneticisi nesne;
    // Tekil Tasarım Şablonu Adım 4: static nesne döndürülür
    return nesne;
}

ECZAlisFaturasiYoneticisi &ECZGenelVeriYoneticisi::getAlisFaturalari()
{
    return alisFaturalari;
}

ECZEczaneBilgileriYoneticisi &ECZGenelVeriYoneticisi::getEczaneBilgileri()
{
    return eczaneBilgileri;
}

ECZIlacBilgisiYoneticisi &ECZGenelVeriYoneticisi::getIlacBilgisi()
{
    return ilacBilgisi;
}

ECZIlacAlisBilgileriYoneticisi &ECZGenelVeriYoneticisi::getIlacAlis()
{
    return ilacAlis;
}

ECZTedarikciYonetici &ECZGenelVeriYoneticisi::getTedarikci()
{
    return tedarikci;
}

QDataStream &operator<<(QDataStream &a, ECZGenelVeriYoneticisi &b)
{
    a << b.eczaneBilgileri << b.alisFaturalari << b.ilacBilgisi << b.ilacAlis << b.tedarikci;

    return a;
}

QDataStream &operator>>(QDataStream &a, ECZGenelVeriYoneticisi &b)
{
    a >> b.eczaneBilgileri >> b.alisFaturalari >> b.ilacBilgisi >> b.ilacAlis >> b.tedarikci;

    return a;
}
