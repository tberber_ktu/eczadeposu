#ifndef TANIMLAR_H
#define TANIMLAR_H

#include <QByteArray>
#include <QDate>
#include <QDateTime>
#include <QString>
#include <QTime>
#include <QtCore>

typedef quint64 IdTuru;
typedef qint32 Tamsayi;

typedef double ReelSayi;

typedef float ParaBirimi;

typedef QString Metin;

typedef QDate Tarih;
typedef QTime Saat;
typedef QDateTime TarihSaat;

typedef enum {
    ITTablet = 1,
    ITSurup = 2,
    ITAsi = 4,
    ITEfervesan = 8,
    ITKrem = 16,
    ITFitil = 32
} IlacTuru;

typedef QByteArray Resim;

// Pointer Tanımları

class ECZAlisFaturasi;
class ECZEczaneBilgileri;
class ECZIlacAlisBilgileri;
class ECZIlacBilgileri;
class ECZIlacSatisFaturasi;
class ECZSatisBilgileri;
class ECZTedarikci;
class ECZUretilenIlac;

#include <memory>

typedef std::shared_ptr<ECZAlisFaturasi> ECZAlisFaturasiPtr;
typedef std::shared_ptr<ECZEczaneBilgileri> ECZEczaneBilgileriPtr;
typedef std::shared_ptr<ECZIlacAlisBilgileri> ECZIlacAlisBilgileriPtr;
typedef std::shared_ptr<ECZIlacBilgileri> ECZIlacBilgileriPtr;
typedef std::shared_ptr<ECZIlacSatisFaturasi> ECZIlacSatisFaturasiPtr;
typedef std::shared_ptr<ECZSatisBilgileri> ECZSatisBilgileriPtr;
typedef std::shared_ptr<ECZTedarikci> ECZTedarikciPtr;
typedef std::shared_ptr<ECZUretilenIlac> ECZUretilenIlacPtr;

QDataStream &operator<<(QDataStream &stream, const ECZAlisFaturasiPtr &veri);
QDataStream &operator>>(QDataStream &stream, ECZAlisFaturasiPtr &veri);

QDataStream &operator<<(QDataStream &stream, const ECZEczaneBilgileriPtr &veri);
QDataStream &operator>>(QDataStream &stream, ECZEczaneBilgileriPtr &veri);

// ECZIlacBilgileriPtr'yi QDataStream'a aktaran fonksiyon
// Fonksiyonun adı özel << operatörü için kullanılıyor
QDataStream &operator<<(QDataStream &a, const ECZIlacBilgileriPtr &b);

// ECZIlacBilgileriPtr'yi QDataStream'dan okuyan fonksiyon
QDataStream &operator>>(QDataStream &a, ECZIlacBilgileriPtr &b);

QDataStream &operator<<(QDataStream &stream, const ECZIlacAlisBilgileriPtr &veri);
QDataStream &operator>>(QDataStream &stream, ECZIlacAlisBilgileriPtr &veri);

QDataStream &operator<<(QDataStream &stream, const ECZTedarikciPtr &veri);
QDataStream &operator>>(QDataStream &stream, ECZTedarikciPtr &veri);

#endif // TANIMLAR_H
