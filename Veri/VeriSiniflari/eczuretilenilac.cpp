#include "eczuretilenilac.h"

ECZUretilenIlac::ECZUretilenIlac(QObject *parent) : QObject(parent)
{
    
}

IdTuru ECZUretilenIlac::getId() const
{
    return uretilenIlacId;
}

void ECZUretilenIlac::setId(const IdTuru &value)
{
    if (value == uretilenIlacId)
        return;
    uretilenIlacId = value;
    emit idDegisti(uretilenIlacId);
}

TarihSaat ECZUretilenIlac::getSonKullanmaTarihi() const
{
    return sonKullanmaTarihi;
}

void ECZUretilenIlac::setSonKullanmaTarihi(const TarihSaat &value)
{
    if (value == sonKullanmaTarihi)
        return;
    sonKullanmaTarihi = value;
    emit sonKullanmaTarihiDegisti(sonKullanmaTarihi);
}

ReelSayi ECZUretilenIlac::getEtkenMaddeMiktari() const
{
    return etkenMaddeMiktari;
}

void ECZUretilenIlac::setEtkenMaddeMiktari(const ReelSayi &value)
{
    if (qFuzzyCompare(value, etkenMaddeMiktari))
        return;
    etkenMaddeMiktari = value;
    emit etkenMaddeMiktariDegisti(etkenMaddeMiktari);
}

Resim ECZUretilenIlac::getKareKodu() const
{
    return kareKodu;
}

void ECZUretilenIlac::setKareKodu(const Resim &value)
{
    if (value == kareKodu)
        return;
    kareKodu = value;
    emit kareKoduDegisti(kareKodu);
}

Metin ECZUretilenIlac::getSeriNumarasi() const
{
    return seriNumarasi;
}

void ECZUretilenIlac::setSeriNumarasi(const Metin &value)
{
    if (value == seriNumarasi)
        return;
    seriNumarasi = value;
    emit seriNumarasiDegisti(seriNumarasi);
}

QDataStream &operator<<(QDataStream &stream, const ECZUretilenIlacPtr &veri)
{
    stream << veri->getId() << veri->getEtkenMaddeMiktari() << veri->getKareKodu()
           << veri->getSeriNumarasi() << veri->getSonKullanmaTarihi();
    return stream;
}

QDataStream &operator>>(QDataStream &stream, ECZUretilenIlacPtr &veri)
{
    IdTuru id;
    TarihSaat tarih;
    ReelSayi miktar;
    Resim kod;
    Metin seri;

    stream >> id >> miktar >> kod >> seri >> tarih;

    veri = std::make_shared<ECZUretilenIlac>();

    veri->setId(id);
    veri->setEtkenMaddeMiktari(miktar);
    veri->setKareKodu(kod);
    veri->setSeriNumarasi(seri);
    veri->setSonKullanmaTarihi(tarih);

    return stream;
}
