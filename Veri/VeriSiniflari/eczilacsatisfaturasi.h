#ifndef ECZILACSATISFATURASI_H
#define ECZILACSATISFATURASI_H

#include <Veri/tanimlar.h>
#include <QObject>

class ECZIlacSatisFaturasi : public QObject
{
    Q_OBJECT
public:
    explicit ECZIlacSatisFaturasi(QObject *parent = nullptr);

    Q_PROPERTY(IdTuru id READ getId WRITE setId NOTIFY idDegisti)
    Q_PROPERTY(TarihSaat faturaTarihi READ getFaturaTarihi WRITE setFaturaTarihi NOTIFY
                   faturaTarihiDegisti)
    Q_PROPERTY(Metin faturaNo READ getFaturaNo WRITE setFaturaNo NOTIFY faturaNoDegisti)

    IdTuru getId() const;
    TarihSaat getFaturaTarihi() const;
    Metin getFaturaNo() const;

signals:
    void idDegisti(const IdTuru &value);
    void faturaTarihiDegisti(const TarihSaat &value);
    void faturaNoDegisti(const Metin &value);

public slots:
    void setId(const IdTuru &value);
    void setFaturaTarihi(const TarihSaat &value);
    void setFaturaNo(const Metin &value);

private:
    IdTuru satisFaturaId;
    TarihSaat faturaTarihi;
    Metin faturaNo;
};

QDataStream &operator<<(QDataStream &stream, const ECZIlacSatisFaturasiPtr &veri);
QDataStream &operator>>(QDataStream &stream, ECZIlacSatisFaturasiPtr &veri);

#endif // ECZILACSATISFATURASI_H
