#ifndef ECZALISFATURASI_H
#define ECZALISFATURASI_H

#include <Veri/tanimlar.h>
#include <QObject>

class ECZAlisFaturasi : public QObject
{
    Q_OBJECT
public:
    explicit ECZAlisFaturasi(QObject *parent = nullptr);

    Q_PROPERTY(IdTuru id READ getId WRITE setId NOTIFY idDegisti)
    Q_PROPERTY(TarihSaat faturaTarihi READ getFaturaTarihi WRITE setFaturaTarihi NOTIFY
                   faturaTarihiDegisti)
    Q_PROPERTY(Metin faturaNo READ getFaturaNo WRITE setFaturaNo NOTIFY faturaNoDegisti)

    IdTuru getId() const;
    TarihSaat getFaturaTarihi() const;
    Metin getFaturaNo() const;

    // ilişkili veriler
    IdTuru getTedarikci() const;
    ECZTedarikciPtr getTedarikciPointer() const;

    QList<ECZIlacAlisBilgileriPtr> getAlinanIlaclar();

signals:
    void idDegisti(const IdTuru &value);
    void faturaTarihiDegisti(const TarihSaat &value);
    void faturaNoDegisti(const Metin &value);

public slots:
    void setId(const IdTuru value);
    void setFaturaTarihi(const TarihSaat &value);
    void setFaturaNo(const Metin &value);

    // ilişkili veriler
    void setTedarikci(const IdTuru value);

private:
    IdTuru alisFaturaId;
    TarihSaat faturaTarihi;
    Metin faturaNo;

    // İlişkili Veriler için gerekli Alanlar
    IdTuru tedarikci;
};

QDataStream &operator<<(QDataStream &stream, const ECZAlisFaturasiPtr &veri);
QDataStream &operator>>(QDataStream &stream, ECZAlisFaturasiPtr &veri);

#endif // ECZALISFATURASI_H
