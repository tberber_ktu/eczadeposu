#include "eczeczanebilgileri.h"

ECZEczaneBilgileri::ECZEczaneBilgileri(QObject *parent) : QObject(parent)
{
    
}

IdTuru ECZEczaneBilgileri::getId() const
{
    return eczaneId;
}

void ECZEczaneBilgileri::setId(const IdTuru &value)
{
    if (value == eczaneId)
        return;
    eczaneId = value;
    emit idDegisti(eczaneId);
}

Metin ECZEczaneBilgileri::getEczaneAdi() const
{
    return eczaneAdi;
}

void ECZEczaneBilgileri::setEczaneAdi(const Metin &value)
{
    if (value == eczaneAdi)
        return;
    eczaneAdi = value;
    emit eczaneAdiDegisti(eczaneAdi);
}

Metin ECZEczaneBilgileri::getEczaneAdresi() const
{
    return eczaneAdresi;
}

void ECZEczaneBilgileri::setEczaneAdresi(const Metin &value)
{
    if (value == eczaneAdresi)
        return;
    eczaneAdresi = value;
    emit eczaneAdresiDegisti(eczaneAdresi);
}

Metin ECZEczaneBilgileri::getEczaneYetkilisi() const
{
    return eczaneYetkilisi;
}

void ECZEczaneBilgileri::setEczaneYetkilisi(const Metin &value)
{
    if (value == eczaneYetkilisi)
        return;
    eczaneYetkilisi = value;
    emit eczaneYetkilisiDegisti(eczaneYetkilisi);
}

Metin ECZEczaneBilgileri::getEczaneTelefonu() const
{
    return eczaneTelefonu;
}

void ECZEczaneBilgileri::setEczaneTelefonu(const Metin &value)
{
    if (value == eczaneTelefonu)
        return;
    eczaneTelefonu = value;
    emit eczaneTelefonuDegisti(eczaneTelefonu);
}

QDataStream &operator<<(QDataStream &stream, const ECZEczaneBilgileriPtr &veri)
{
    stream << veri->getId() << veri->getEczaneAdi() << veri->getEczaneAdresi()
           << veri->getEczaneTelefonu() << veri->getEczaneYetkilisi();
    return stream;
}

QDataStream &operator>>(QDataStream &stream, ECZEczaneBilgileriPtr &veri)
{
    IdTuru id;
    Metin eczaneAdi, eczaneAdresi, eczaneTelefonu, eczaneYetkilisi;

    stream >> id >> eczaneAdi >> eczaneAdresi >> eczaneTelefonu >> eczaneYetkilisi;

    veri = std::make_shared<ECZEczaneBilgileri>();

    veri->setId(id);
    veri->setEczaneAdi(eczaneAdi);
    veri->setEczaneAdresi(eczaneAdresi);
    veri->setEczaneTelefonu(eczaneTelefonu);
    veri->setEczaneYetkilisi(eczaneYetkilisi);

    return stream;
}
