#ifndef ECZTEDARIKCI_H
#define ECZTEDARIKCI_H

#include <Veri/tanimlar.h>
#include <QObject>

class ECZTedarikci : public QObject
{
    Q_OBJECT
public:
    explicit ECZTedarikci(QObject *parent = nullptr);

    Q_PROPERTY(IdTuru id READ getId WRITE setId NOTIFY idDegisti)
    Q_PROPERTY(
        Metin tedarikciAdi READ getTedarikciAdi WRITE setTedarikciAdi NOTIFY tedarikciAdiDegisti)
    Q_PROPERTY(Metin tedarikciAdresi READ getTedarikciAdresi WRITE setTedarikciAdresi NOTIFY
                   tedarikciAdresiDegisti)
    Q_PROPERTY(Metin tedarikciTelefonu READ getTedarikciTelefonu WRITE setTedarikciTelefonu NOTIFY
                   tedarikciTelefonuDegisti)
    Q_PROPERTY(Metin tedarikciYetkiliKisi READ getTedarikciYetkiliKisi WRITE setTedarikciYetkiliKisi
                   NOTIFY tedarikciYetkiliKisiDegisti)

    IdTuru getId() const;
    Metin getTedarikciAdi() const;
    Metin getTedarikciAdresi() const;
    Metin getTedarikciTelefonu() const;
    Metin getTedarikciYetkiliKisi() const;

signals:
    void idDegisti(const IdTuru &value);
    void tedarikciAdiDegisti(const Metin &value);
    void tedarikciAdresiDegisti(const Metin &value);
    void tedarikciTelefonuDegisti(const Metin &value);
    void tedarikciYetkiliKisiDegisti(const Metin &value);

public slots:
    void setId(const IdTuru &value);
    void setTedarikciAdi(const Metin &value);
    void setTedarikciAdresi(const Metin &value);
    void setTedarikciTelefonu(const Metin &value);
    void setTedarikciYetkiliKisi(const Metin &value);

private:
    IdTuru tedarikciId;
    Metin tedarikciAdi;
    Metin tedarikciAdresi;
    Metin tedarikciTelefonu;
    Metin tedarikciYetkiliKisi;
};

QDataStream &operator<<(QDataStream &stream, const ECZTedarikciPtr &veri);
QDataStream &operator>>(QDataStream &stream, ECZTedarikciPtr &veri);

#endif // ECZTEDARIKCI_H
