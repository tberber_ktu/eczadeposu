#ifndef ECZILACALISBILGILERI_H
#define ECZILACALISBILGILERI_H

#include <Veri/tanimlar.h>
#include <QObject>

class ECZIlacAlisBilgileri : public QObject
{
    Q_OBJECT
public:
    explicit ECZIlacAlisBilgileri(QObject *parent = nullptr);

    Q_PROPERTY(IdTuru id READ getId WRITE setId NOTIFY idDegisti)
    Q_PROPERTY(ReelSayi ilacAlisMiktari READ getIlacAlisMiktari WRITE setIlacAlisMiktari NOTIFY
                   ilacAlisMiktariDegisti)
    Q_PROPERTY(ParaBirimi birimFiyat READ getBirimFiyat WRITE setBirimFiyat NOTIFY birimFiyatDegisti)
    Q_PROPERTY(ParaBirimi toplam READ getToplam)

    IdTuru getId() const;
    ReelSayi getIlacAlisMiktari() const;
    ParaBirimi getBirimFiyat() const;
    ParaBirimi getToplam() const;

    IdTuru getIlacId() const;
    ECZIlacBilgileriPtr getIlac();

    IdTuru getAlisFaturaId() const;
    ECZAlisFaturasiPtr getFatura();

signals:
    void idDegisti(const IdTuru &value);
    void ilacAlisMiktariDegisti(const ReelSayi &value);
    void birimFiyatDegisti(const ParaBirimi &value);

public slots:
    void setId(const IdTuru &value);
    void setIlacAlisMiktari(const ReelSayi &value);
    void setBirimFiyat(const ParaBirimi &value);

    void setIlacId(const IdTuru &value);
    void setAlisFaturaId(const IdTuru &value);

private:
    IdTuru ilacAlisBilgiId;
    ReelSayi ilacAlisMiktari;
    ParaBirimi birimFiyat;

    // ilişkili veriler
    IdTuru ilacId;
    IdTuru alisFaturaId;
};

QDataStream &operator<<(QDataStream &stream, const ECZIlacAlisBilgileriPtr &veri);
QDataStream &operator>>(QDataStream &stream, ECZIlacAlisBilgileriPtr &veri);

#endif // ECZILACALISBILGILERI_H
