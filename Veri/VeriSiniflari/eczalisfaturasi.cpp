#include "eczalisfaturasi.h"

#include <Veri/VeriSiniflari/eczilacalisbilgileri.h>
#include <Veri/VeriSiniflari/ecztedarikci.h>
#include <Veri/eczgenelveriyoneticisi.h>

ECZAlisFaturasi::ECZAlisFaturasi(QObject *parent) : QObject(parent)
{
    
}

IdTuru ECZAlisFaturasi::getId() const
{
    return alisFaturaId;
}

void ECZAlisFaturasi::setId(const IdTuru value)
{
    if (value == alisFaturaId)
        return;
    alisFaturaId = value;
    emit idDegisti(alisFaturaId);
}

TarihSaat ECZAlisFaturasi::getFaturaTarihi() const
{
    return faturaTarihi;
}

void ECZAlisFaturasi::setFaturaTarihi(const TarihSaat &value)
{
    if (value == faturaTarihi)
        return;
    faturaTarihi = value;
    emit faturaTarihiDegisti(value);
}

Metin ECZAlisFaturasi::getFaturaNo() const
{
    return faturaNo;
}

IdTuru ECZAlisFaturasi::getTedarikci() const
{
    return tedarikci;
}

ECZTedarikciPtr ECZAlisFaturasi::getTedarikciPointer() const
{
    IdTuru tedarikciId = tedarikci;
    auto tedarikciPtr = ECZGenelVeriYoneticisi::sec().getTedarikci().ilkiniBul(
        [tedarikciId](ECZTedarikciPtr veri) { return veri->getId() == tedarikciId; });
    return tedarikciPtr;
}

QList<ECZIlacAlisBilgileriPtr> ECZAlisFaturasi::getAlinanIlaclar()
{
    auto faturaIDsi = getId();
    auto ilaclar = ECZGenelVeriYoneticisi::sec().getIlacAlis().tumunuBul(
        [faturaIDsi](ECZIlacAlisBilgileriPtr veri) {
            return veri->getAlisFaturaId() == faturaIDsi;
        });

    return ilaclar;
}

void ECZAlisFaturasi::setFaturaNo(const Metin &value)
{
    if (value == faturaNo)
        return;
    faturaNo = value;
    emit faturaNoDegisti(faturaNo);
}

void ECZAlisFaturasi::setTedarikci(const IdTuru value)
{
    if (value != tedarikci) {
        tedarikci = value;
    }
}

QDataStream &operator<<(QDataStream &stream, const ECZAlisFaturasiPtr &veri)
{
    stream << veri->getId() << veri->getFaturaNo() << veri->getFaturaTarihi();
    return stream;
}

QDataStream &operator>>(QDataStream &stream, ECZAlisFaturasiPtr &veri)
{
    IdTuru id;
    Metin faturaNo;
    TarihSaat faturaTarihi;

    stream >> id >> faturaNo >> faturaTarihi;

    veri = std::make_shared<ECZAlisFaturasi>();

    veri->setId(id);
    veri->setFaturaNo(faturaNo);
    veri->setFaturaTarihi(faturaTarihi);

    return stream;
}
