#ifndef ECZECZANEBILGILERI_H
#define ECZECZANEBILGILERI_H

#include <Veri/tanimlar.h>
#include <QObject>

class ECZEczaneBilgileri : public QObject
{
    Q_OBJECT
public:
    explicit ECZEczaneBilgileri(QObject *parent = nullptr);

    Q_PROPERTY(IdTuru id READ getId WRITE setId NOTIFY idDegisti)
    Q_PROPERTY(Metin eczaneAdi READ getEczaneAdi WRITE setEczaneAdi NOTIFY eczaneAdiDegisti)
    Q_PROPERTY(
        Metin eczaneAdresi READ getEczaneAdresi WRITE setEczaneAdresi NOTIFY eczaneAdresiDegisti)
    Q_PROPERTY(Metin eczaneYetkilisi READ getEczaneYetkilisi WRITE setEczaneYetkilisi NOTIFY
                   eczaneYetkilisiDegisti)
    Q_PROPERTY(Metin eczaneTelefonu READ getEczaneTelefonu WRITE setEczaneTelefonu NOTIFY
                   eczaneTelefonuDegisti)

    IdTuru getId() const;
    Metin getEczaneAdi() const;
    Metin getEczaneAdresi() const;
    Metin getEczaneYetkilisi() const;
    Metin getEczaneTelefonu() const;

signals:
    void idDegisti(const IdTuru &value);
    void eczaneAdiDegisti(const Metin &value);
    void eczaneAdresiDegisti(const Metin &value);
    void eczaneYetkilisiDegisti(const Metin &value);
    void eczaneTelefonuDegisti(const Metin &value);

public slots:
    void setId(const IdTuru &value);
    void setEczaneAdi(const Metin &value);
    void setEczaneAdresi(const Metin &value);
    void setEczaneYetkilisi(const Metin &value);
    void setEczaneTelefonu(const Metin &value);

private:
    IdTuru eczaneId;
    Metin eczaneAdi;
    Metin eczaneAdresi;
    Metin eczaneYetkilisi;
    Metin eczaneTelefonu;
};

QDataStream &operator<<(QDataStream &stream, const ECZEczaneBilgileriPtr &veri);
QDataStream &operator>>(QDataStream &stream, ECZEczaneBilgileriPtr &veri);

#endif // ECZECZANEBILGILERI_H
