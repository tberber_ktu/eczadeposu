#include "eczilacbilgileri.h"

#include <QtMath>

ECZIlacBilgileri::ECZIlacBilgileri(QObject *parent) : QObject(parent)
{
    
}

IdTuru ECZIlacBilgileri::getId() const
{
    return ilacId;
}

void ECZIlacBilgileri::setId(const IdTuru &value)
{
    if (value == ilacId)
        return;
    ilacId = value;
    emit idDegisti(ilacId);
}

Metin ECZIlacBilgileri::getIlacAdi() const
{
    return ilacAdi;
}

void ECZIlacBilgileri::setIlacAdi(const Metin &value)
{
    if (value == ilacAdi)
        return;
    ilacAdi = value;
    emit ilacAdiDegisti(ilacAdi);
}

IlacTuru ECZIlacBilgileri::getIlacTuru() const
{
    return ilacTuru;
}

void ECZIlacBilgileri::setIlacTuru(const IlacTuru &value)
{
    if (value == ilacTuru)
        return;
    ilacTuru = value;
    emit ilacTuruDegisti(ilacTuru);
}

Metin ECZIlacBilgileri::getIlacYanEtkiler() const
{
    return ilacYanEtkiler;
}

void ECZIlacBilgileri::setIlacYanEtkiler(const Metin &value)
{
    if (value == ilacYanEtkiler)
        return;
    ilacYanEtkiler = value;
    emit ilacYanEtkilerDegisti(ilacYanEtkiler);
}

Metin ECZIlacBilgileri::getIlacEtkenMaddesi() const
{
    return ilacEtkenMaddesi;
}

void ECZIlacBilgileri::setIlacEtkenMaddesi(const Metin &value)
{
    if (value == ilacEtkenMaddesi)
        return;
    ilacEtkenMaddesi = value;
    emit ilacEtkenMaddesiDegisti(ilacEtkenMaddesi);
}

ReelSayi ECZIlacBilgileri::getIlacEtkenMaddeMiktari() const
{
    return ilacEtkenMaddeMiktari;
}

void ECZIlacBilgileri::setIlacEtkenMaddeMiktari(const ReelSayi &value)
{
    if (qFuzzyCompare(ilacEtkenMaddeMiktari, value))
        return;
    ilacEtkenMaddeMiktari = value;
    emit ilacEtkenMaddeMiktariDegisti(ilacEtkenMaddeMiktari);
}

QDataStream &operator<<(QDataStream &a, const ECZIlacBilgileriPtr &b)
{
    a << b->getId() << b->getIlacAdi() << b->getIlacEtkenMaddeMiktari() << b->getIlacEtkenMaddesi()
      << b->getIlacTuru() << b->getIlacYanEtkiler();

    return a;
}

QDataStream &operator>>(QDataStream &a, ECZIlacBilgileriPtr &b)
{
    IdTuru ilacId;
    Metin ilacAdi;
    IlacTuru ilacTuru;
    Metin ilacYanEtkiler;
    Metin ilacEtkenMaddesi;
    ReelSayi ilacEtkenMaddeMiktari;

    a >> ilacId >> ilacAdi >> ilacEtkenMaddeMiktari >> ilacEtkenMaddesi >> ilacTuru
        >> ilacYanEtkiler;

    b = std::make_shared<ECZIlacBilgileri>();

    b->setId(ilacId);
    b->setIlacAdi(ilacAdi);
    b->setIlacTuru(ilacTuru);
    b->setIlacYanEtkiler(ilacYanEtkiler);
    b->setIlacEtkenMaddesi(ilacEtkenMaddesi);
    b->setIlacEtkenMaddeMiktari(ilacEtkenMaddeMiktari);

    return a;
}
