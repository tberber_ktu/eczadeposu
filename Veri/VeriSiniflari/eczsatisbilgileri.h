#ifndef ECZSATISBILGILERI_H
#define ECZSATISBILGILERI_H

#include <Veri/tanimlar.h>
#include <QObject>

class ECZSatisBilgileri : public QObject
{
    Q_OBJECT
public:
    explicit ECZSatisBilgileri(QObject *parent = nullptr);

    Q_PROPERTY(IdTuru id READ getId WRITE setId NOTIFY idDegisti)
    Q_PROPERTY(ReelSayi ilacSatisMiktari READ getIlacSatisMiktari WRITE setIlacSatisMiktari NOTIFY
                   ilacSatisMiktariDegisti)
    Q_PROPERTY(ParaBirimi birimFiyat READ getBirimFiyat WRITE setBirimFiyat NOTIFY birimFiyatDegisti)
    Q_PROPERTY(ParaBirimi toplamFiyat READ getToplamFiyat)

    IdTuru getId() const;
    ReelSayi getIlacSatisMiktari() const;
    ParaBirimi getBirimFiyat() const;
    ParaBirimi getToplamFiyat() const;

signals:
    void idDegisti(const IdTuru &value);
    void ilacSatisMiktariDegisti(const ReelSayi &value);
    void birimFiyatDegisti(const ParaBirimi &value);

public slots:
    void setId(const IdTuru &value);
    void setIlacSatisMiktari(const ReelSayi &value);
    void setBirimFiyat(const ParaBirimi &value);

private:
    IdTuru ilacSatisBilgiId;
    ReelSayi ilacSatisMiktari;
    ParaBirimi birimFiyat;
};

QDataStream &operator<<(QDataStream &stream, const ECZSatisBilgileriPtr &veri);
QDataStream &operator>>(QDataStream &stream, ECZSatisBilgileriPtr &veri);

#endif // ECZSATISBILGILERI_H
