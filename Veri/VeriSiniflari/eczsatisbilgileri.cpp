#include "eczsatisbilgileri.h"

ECZSatisBilgileri::ECZSatisBilgileri(QObject *parent) : QObject(parent)
{
    
}

IdTuru ECZSatisBilgileri::getId() const
{
    return ilacSatisBilgiId;
}

void ECZSatisBilgileri::setId(const IdTuru &value)
{
    if (value == ilacSatisBilgiId)
        return;
    ilacSatisBilgiId = value;
    emit idDegisti(ilacSatisBilgiId);
}

ReelSayi ECZSatisBilgileri::getIlacSatisMiktari() const
{
    return ilacSatisMiktari;
}

void ECZSatisBilgileri::setIlacSatisMiktari(const ReelSayi &value)
{
    if (ilacSatisMiktari == value)
        return;
    ilacSatisMiktari = value;
    emit ilacSatisMiktariDegisti(ilacSatisMiktari);
}

ParaBirimi ECZSatisBilgileri::getBirimFiyat() const
{
    return birimFiyat;
}

ParaBirimi ECZSatisBilgileri::getToplamFiyat() const
{
    return birimFiyat * ilacSatisMiktari;
}

void ECZSatisBilgileri::setBirimFiyat(const ParaBirimi &value)
{
    if (birimFiyat == value)
        return;
    birimFiyat = value;
    emit birimFiyatDegisti(birimFiyat);
}

QDataStream &operator<<(QDataStream &stream, const ECZSatisBilgileriPtr &veri)
{
    stream << veri->getId() << veri->getBirimFiyat() << veri->getIlacSatisMiktari();
    return stream;
}

QDataStream &operator>>(QDataStream &stream, ECZSatisBilgileriPtr &veri)
{
    IdTuru id;
    ReelSayi miktar;
    ParaBirimi fiyat;

    stream >> id >> fiyat >> miktar;

    veri = std::make_shared<ECZSatisBilgileri>();

    veri->setId(id);
    veri->setBirimFiyat(fiyat);
    veri->setIlacSatisMiktari(miktar);

    return stream;
}
