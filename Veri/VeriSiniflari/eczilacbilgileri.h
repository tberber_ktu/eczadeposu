#ifndef ECZILACBILGILERI_H
#define ECZILACBILGILERI_H

#include <QObject>

#include <Veri/tanimlar.h>

// Plain old C++ Objects (POCO)
class ECZIlacBilgileri : public QObject
{
    Q_OBJECT
public:
    explicit ECZIlacBilgileri(QObject *parent = nullptr);

    Q_PROPERTY(IdTuru id READ getId WRITE setId NOTIFY idDegisti)
    Q_PROPERTY(Metin ilacAdi READ getIlacAdi WRITE setIlacAdi NOTIFY ilacAdiDegisti)
    Q_PROPERTY(IlacTuru ilacTuru READ getIlacTuru WRITE setIlacTuru NOTIFY ilacTuruDegisti)
    Q_PROPERTY(Metin ilacYanEtkiler READ getIlacYanEtkiler WRITE setIlacYanEtkiler NOTIFY
                   ilacYanEtkilerDegisti)
    Q_PROPERTY(Metin ilacEtkenMaddesi READ getIlacEtkenMaddesi WRITE setIlacEtkenMaddesi NOTIFY
                   ilacEtkenMaddesiDegisti)
    Q_PROPERTY(ReelSayi ilacEtkenMaddeMiktari READ getIlacEtkenMaddeMiktari WRITE
                   setIlacEtkenMaddeMiktari NOTIFY ilacEtkenMaddeMiktariDegisti)

    IdTuru getId() const;
    Metin getIlacAdi() const;
    IlacTuru getIlacTuru() const;
    Metin getIlacYanEtkiler() const;
    Metin getIlacEtkenMaddesi() const;
    ReelSayi getIlacEtkenMaddeMiktari() const;

signals:
    void idDegisti(const IdTuru &value);
    void ilacAdiDegisti(const Metin &value);
    void ilacTuruDegisti(const IlacTuru &value);
    void ilacYanEtkilerDegisti(const Metin &value);
    void ilacEtkenMaddesiDegisti(const Metin &value);
    void ilacEtkenMaddeMiktariDegisti(const ReelSayi &value);

public slots:
    void setId(const IdTuru &value);
    void setIlacAdi(const Metin &value);
    void setIlacTuru(const IlacTuru &value);
    void setIlacYanEtkiler(const Metin &value);
    void setIlacEtkenMaddesi(const Metin &value);
    void setIlacEtkenMaddeMiktari(const ReelSayi &value);

private:
    IdTuru ilacId;
    Metin ilacAdi;
    IlacTuru ilacTuru;
    Metin ilacYanEtkiler;
    Metin ilacEtkenMaddesi;
    ReelSayi ilacEtkenMaddeMiktari;
};

// ECZIlacBilgileriPtr'yi QDataStream'a aktaran fonksiyon
// Fonksiyonun adı özel << operatörü için kullanılıyor
QDataStream &operator<<(QDataStream &a, const ECZIlacBilgileriPtr &b);

// ECZIlacBilgileriPtr'yi QDataStream'dan okuyan fonksiyon
QDataStream &operator>>(QDataStream &a, ECZIlacBilgileriPtr &b);

#endif // ECZILACBILGILERI_H
