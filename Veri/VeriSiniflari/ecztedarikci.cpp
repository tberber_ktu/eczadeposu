#include "ecztedarikci.h"

ECZTedarikci::ECZTedarikci(QObject *parent) : QObject(parent)
{
    
}

IdTuru ECZTedarikci::getId() const
{
    return tedarikciId;
}

void ECZTedarikci::setId(const IdTuru &value)
{
    if (value == tedarikciId)
        return;
    tedarikciId = value;
    emit idDegisti(tedarikciId);
}

Metin ECZTedarikci::getTedarikciAdi() const
{
    return tedarikciAdi;
}

void ECZTedarikci::setTedarikciAdi(const Metin &value)
{
    if (value == tedarikciAdi)
        return;
    tedarikciAdi = value;
    emit tedarikciAdiDegisti(tedarikciAdi);
}

Metin ECZTedarikci::getTedarikciAdresi() const
{
    return tedarikciAdresi;
}

void ECZTedarikci::setTedarikciAdresi(const Metin &value)
{
    if (value == tedarikciAdresi)
        return;
    tedarikciAdresi = value;
    emit tedarikciAdresiDegisti(tedarikciAdresi);
}

Metin ECZTedarikci::getTedarikciTelefonu() const
{
    return tedarikciTelefonu;
}

void ECZTedarikci::setTedarikciTelefonu(const Metin &value)
{
    if (value == tedarikciTelefonu)
        return;
    tedarikciTelefonu = value;
    emit tedarikciTelefonuDegisti(tedarikciTelefonu);
}

Metin ECZTedarikci::getTedarikciYetkiliKisi() const
{
    return tedarikciYetkiliKisi;
}

void ECZTedarikci::setTedarikciYetkiliKisi(const Metin &value)
{
    if (value == tedarikciYetkiliKisi)
        return;
    tedarikciYetkiliKisi = value;
    emit tedarikciYetkiliKisiDegisti(tedarikciYetkiliKisi);
}

QDataStream &operator<<(QDataStream &stream, const ECZTedarikciPtr &veri)
{
    stream << veri->getId() << veri->getTedarikciAdi() << veri->getTedarikciAdresi()
           << veri->getTedarikciTelefonu() << veri->getTedarikciYetkiliKisi();
    return stream;
}

QDataStream &operator>>(QDataStream &stream, ECZTedarikciPtr &veri)
{
    IdTuru id;
    Metin adi, adresi, telefon, yetkili;

    stream >> id >> adi >> adresi >> telefon >> yetkili;

    veri = std::make_shared<ECZTedarikci>();

    veri->setId(id);
    veri->setTedarikciAdi(adi);
    veri->setTedarikciAdresi(adresi);
    veri->setTedarikciTelefonu(telefon);
    veri->setTedarikciYetkiliKisi(yetkili);

    return stream;
}
