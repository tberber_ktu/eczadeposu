#include "eczilacalisbilgileri.h"

#include <Veri/VeriSiniflari/eczalisfaturasi.h>
#include <Veri/VeriSiniflari/eczilacbilgileri.h>
#include <Veri/eczgenelveriyoneticisi.h>

ECZIlacAlisBilgileri::ECZIlacAlisBilgileri(QObject *parent) : QObject(parent)
{
    
}

IdTuru ECZIlacAlisBilgileri::getId() const
{
    return ilacAlisBilgiId;
}

void ECZIlacAlisBilgileri::setId(const IdTuru &value)
{
    if (value == ilacAlisBilgiId)
        return;
    ilacAlisBilgiId = value;
    emit idDegisti(ilacAlisBilgiId);
}

ReelSayi ECZIlacAlisBilgileri::getIlacAlisMiktari() const
{
    return ilacAlisMiktari;
}

void ECZIlacAlisBilgileri::setIlacAlisMiktari(const ReelSayi &value)
{
    if (qFuzzyCompare(value, ilacAlisMiktari))
        return;
    ilacAlisMiktari = value;
    emit ilacAlisMiktariDegisti(ilacAlisMiktari);
}

ParaBirimi ECZIlacAlisBilgileri::getBirimFiyat() const
{
    return birimFiyat;
}

ParaBirimi ECZIlacAlisBilgileri::getToplam() const
{
    return ilacAlisMiktari * birimFiyat;
}

void ECZIlacAlisBilgileri::setBirimFiyat(const ParaBirimi &value)
{
    if (qFuzzyCompare(value, birimFiyat))
        return;
    birimFiyat = value;
    emit birimFiyatDegisti(birimFiyat);
}

IdTuru ECZIlacAlisBilgileri::getAlisFaturaId() const
{
    return alisFaturaId;
}

ECZAlisFaturasiPtr ECZIlacAlisBilgileri::getFatura()
{
    IdTuru faturaId = alisFaturaId;
    auto faturaPtr = ECZGenelVeriYoneticisi::sec().getAlisFaturalari().ilkiniBul(
        [faturaId](ECZAlisFaturasiPtr veri) { return veri->getId() == faturaId; });
    return faturaPtr;
}

void ECZIlacAlisBilgileri::setAlisFaturaId(const IdTuru &value)
{
    alisFaturaId = value;
}

IdTuru ECZIlacAlisBilgileri::getIlacId() const
{
    return ilacId;
}

ECZIlacBilgileriPtr ECZIlacAlisBilgileri::getIlac()
{
    IdTuru ilacId = this->ilacId;
    auto ilacPtr = ECZGenelVeriYoneticisi::sec().getIlacBilgisi().ilkiniBul(
        [ilacId](ECZIlacBilgileriPtr veri) { return veri->getId() == ilacId; });
    return ilacPtr;
}

void ECZIlacAlisBilgileri::setIlacId(const IdTuru &value)
{
    ilacId = value;
}

QDataStream &operator<<(QDataStream &stream, const ECZIlacAlisBilgileriPtr &veri)
{
    stream << veri->getId() << veri->getBirimFiyat() << veri->getIlacAlisMiktari();
    return stream;
}

QDataStream &operator>>(QDataStream &stream, ECZIlacAlisBilgileriPtr &veri)
{
    IdTuru id;
    ReelSayi miktar;
    ParaBirimi birimFiyat;

    stream >> id >> birimFiyat >> miktar;

    veri = std::make_shared<ECZIlacAlisBilgileri>();

    veri->setId(id);
    veri->setBirimFiyat(birimFiyat);
    veri->setIlacAlisMiktari(miktar);

    return stream;
}
