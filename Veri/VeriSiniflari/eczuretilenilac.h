#ifndef ECZURETILENILAC_H
#define ECZURETILENILAC_H

#include <Veri/tanimlar.h>
#include <QObject>

class ECZUretilenIlac : public QObject
{
    Q_OBJECT
public:
    explicit ECZUretilenIlac(QObject *parent = nullptr);

    Q_PROPERTY(IdTuru id READ getId WRITE setId NOTIFY idDegisti)
    Q_PROPERTY(TarihSaat sonKullanmaTarihi READ getSonKullanmaTarihi WRITE setSonKullanmaTarihi
                   NOTIFY sonKullanmaTarihiDegisti)
    Q_PROPERTY(ReelSayi etkenMaddeMiktari READ getEtkenMaddeMiktari WRITE setEtkenMaddeMiktari
                   NOTIFY etkenMaddeMiktariDegisti)
    Q_PROPERTY(Resim kareKodu READ getKareKodu WRITE setKareKodu NOTIFY kareKoduDegisti)
    Q_PROPERTY(
        Metin seriNumarasi READ getSeriNumarasi WRITE setSeriNumarasi NOTIFY seriNumarasiDegisti)

    IdTuru getId() const;
    TarihSaat getSonKullanmaTarihi() const;
    ReelSayi getEtkenMaddeMiktari() const;
    Resim getKareKodu() const;
    Metin getSeriNumarasi() const;

signals:
    void idDegisti(const IdTuru &value);
    void sonKullanmaTarihiDegisti(const TarihSaat &value);
    void etkenMaddeMiktariDegisti(const ReelSayi &value);
    void kareKoduDegisti(const Resim &value);
    void seriNumarasiDegisti(const Metin &value);

public slots:
    void setId(const IdTuru &value);
    void setSonKullanmaTarihi(const TarihSaat &value);
    void setEtkenMaddeMiktari(const ReelSayi &value);
    void setKareKodu(const Resim &value);
    void setSeriNumarasi(const Metin &value);

private:
    IdTuru uretilenIlacId;
    TarihSaat sonKullanmaTarihi;
    ReelSayi etkenMaddeMiktari;
    Resim kareKodu;
    Metin seriNumarasi;
};

QDataStream &operator<<(QDataStream &stream, const ECZUretilenIlacPtr &veri);
QDataStream &operator>>(QDataStream &stream, ECZUretilenIlacPtr &veri);

#endif // ECZURETILENILAC_H
