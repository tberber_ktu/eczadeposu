#include "eczilacsatisfaturasi.h"

ECZIlacSatisFaturasi::ECZIlacSatisFaturasi(QObject *parent) : QObject(parent) {}

IdTuru ECZIlacSatisFaturasi::getId() const
{
    return satisFaturaId;
}

void ECZIlacSatisFaturasi::setId(const IdTuru &value)
{
    if (value == satisFaturaId)
        return;
    satisFaturaId = value;
    emit idDegisti(satisFaturaId);
}

TarihSaat ECZIlacSatisFaturasi::getFaturaTarihi() const
{
    return faturaTarihi;
}

void ECZIlacSatisFaturasi::setFaturaTarihi(const TarihSaat &value)
{
    if (value == faturaTarihi)
        return;
    faturaTarihi = value;
    emit faturaTarihiDegisti(faturaTarihi);
}

Metin ECZIlacSatisFaturasi::getFaturaNo() const
{
    return faturaNo;
}

void ECZIlacSatisFaturasi::setFaturaNo(const Metin &value)
{
    if (value == faturaNo)
        return;
    faturaNo = value;
    emit faturaNoDegisti(faturaNo);
}

QDataStream &operator<<(QDataStream &stream, const ECZIlacSatisFaturasiPtr &veri)
{
    stream << veri->getId() << veri->getFaturaNo() << veri->getFaturaTarihi();
    return stream;
}

QDataStream &operator>>(QDataStream &stream, ECZIlacSatisFaturasiPtr &veri)
{
    IdTuru id;
    Metin faturaNo;
    TarihSaat faturaTarih;

    stream >> id >> faturaNo >> faturaTarih;

    veri = std::make_shared<ECZIlacSatisFaturasi>();

    veri->setId(id);
    veri->setFaturaNo(faturaNo);
    veri->setFaturaTarihi(faturaTarih);

    return stream;
}
