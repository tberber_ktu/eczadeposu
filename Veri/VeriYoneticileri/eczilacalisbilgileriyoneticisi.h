#ifndef ECZILACALISBILGILERIYONETICISI_H
#define ECZILACALISBILGILERIYONETICISI_H

#include <Veri/VeriYoneticileri/temel_veri_yoneticisi.h>
#include <QObject>

class ECZIlacAlisBilgileriYoneticisi
    : public QObject,
      public TemelVeriYoneticisi<ECZIlacAlisBilgileri, ECZIlacAlisBilgileriPtr>
{
    Q_OBJECT
public:
    explicit ECZIlacAlisBilgileriYoneticisi(QObject *parent = nullptr);

    Ptr kopyaOlustur(Ptr kaynak) const;

signals:

};

#endif // ECZILACALISBILGILERIYONETICISI_H
