#include "eczalisfaturasiyoneticisi.h"
#include <Veri/VeriSiniflari/eczalisfaturasi.h>

ECZAlisFaturasiYoneticisi::ECZAlisFaturasiYoneticisi(QObject *parent) : QObject(parent)
{
    
}

ECZAlisFaturasiYoneticisi::Ptr ECZAlisFaturasiYoneticisi::kopyaOlustur(
    ECZAlisFaturasiYoneticisi::Ptr kaynak) const
{
    Ptr kopya = yeni();

    kopya->setId(kaynak->getId());
    kopya->setFaturaNo(kaynak->getFaturaNo());
    kopya->setFaturaTarihi(kaynak->getFaturaTarihi());

    return kopya;
}
