#include "eczeczanebilgileriyoneticisi.h"
#include <Veri/VeriSiniflari/eczeczanebilgileri.h>

ECZEczaneBilgileriYoneticisi::ECZEczaneBilgileriYoneticisi(QObject *parent) : QObject(parent)
{
    
}

ECZEczaneBilgileriYoneticisi::Ptr ECZEczaneBilgileriYoneticisi::kopyaOlustur(
    TemelVeriYoneticisi::Ptr kaynak) const
{
    Ptr kopya = yeni();

    kopya->setId(kaynak->getId());
    kopya->setEczaneAdi(kaynak->getEczaneAdi());
    kopya->setEczaneAdresi(kaynak->getEczaneAdresi());
    kopya->setEczaneTelefonu(kaynak->getEczaneTelefonu());
    kopya->setEczaneYetkilisi(kaynak->getEczaneYetkilisi());

    return kopya;
}
