#include "eczilacbilgisiyoneticisi.h"

#include <Veri/VeriSiniflari/eczilacbilgileri.h>

ECZIlacBilgisiYoneticisi::ECZIlacBilgisiYoneticisi(QObject *parent)
    : QObject(parent), TemelVeriYoneticisi<ECZIlacBilgileri, ECZIlacBilgileriPtr>()
{
    
}

ECZIlacBilgisiYoneticisi::Ptr ECZIlacBilgisiYoneticisi::kopyaOlustur(
    ECZIlacBilgisiYoneticisi::Ptr kaynak) const
{
    Ptr kopya = yeni();

    kopya->setId(kaynak->getId());
    kopya->setIlacAdi(kaynak->getIlacAdi());
    kopya->setIlacEtkenMaddeMiktari(kaynak->getIlacEtkenMaddeMiktari());
    kopya->setIlacEtkenMaddesi(kaynak->getIlacEtkenMaddesi());
    kopya->setIlacTuru(kaynak->getIlacTuru());
    kopya->setIlacYanEtkiler(kaynak->getIlacYanEtkiler());

    return kopya;
}
