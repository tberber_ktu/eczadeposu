#ifndef ECZTEDARIKCIYONETICI_H
#define ECZTEDARIKCIYONETICI_H

#include <Veri/VeriYoneticileri/temel_veri_yoneticisi.h>
#include <Veri/tanimlar.h>
#include <QObject>

class ECZTedarikciYonetici : public QObject,
                             public TemelVeriYoneticisi<ECZTedarikci, ECZTedarikciPtr>
{
    Q_OBJECT
public:
    explicit ECZTedarikciYonetici(QObject *parent = nullptr);

    Ptr kopyaOlustur(Ptr kaynak) const;

signals:

};

#endif // ECZTEDARIKCIYONETICI_H
