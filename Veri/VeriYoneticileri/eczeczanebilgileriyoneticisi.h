#ifndef ECZECZANEBILGILERIYONETICISI_H
#define ECZECZANEBILGILERIYONETICISI_H

#include <Veri/VeriYoneticileri/temel_veri_yoneticisi.h>
#include <QObject>

class ECZEczaneBilgileriYoneticisi
    : public QObject,
      public TemelVeriYoneticisi<ECZEczaneBilgileri, ECZEczaneBilgileriPtr>
{
    Q_OBJECT
public:
    explicit ECZEczaneBilgileriYoneticisi(QObject *parent = nullptr);

    // 4. İlacın Kopyasını Oluşturulsun

    Ptr kopyaOlustur(Ptr kaynak) const;

signals:

};

#endif // ECZECZANEBILGILERIYONETICISI_H
