#ifndef ECZALISFATURASIYONETICISI_H
#define ECZALISFATURASIYONETICISI_H

#include <Veri/VeriYoneticileri/temel_veri_yoneticisi.h>
#include <QObject>

class ECZAlisFaturasiYoneticisi : public QObject,
                                  public TemelVeriYoneticisi<ECZAlisFaturasi, ECZAlisFaturasiPtr>
{
    Q_OBJECT
public:
    explicit ECZAlisFaturasiYoneticisi(QObject *parent = nullptr);

    // 4. İlacın Kopyasını Oluşturulsun

    Ptr kopyaOlustur(Ptr kaynak) const;

signals:

};

#endif // ECZALISFATURASIYONETICISI_H
