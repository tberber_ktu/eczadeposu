#ifndef ECZILACBILGISIYONETICISI_H
#define ECZILACBILGISIYONETICISI_H

#include <Veri/VeriYoneticileri/temel_veri_yoneticisi.h>
#include <Veri/tanimlar.h>
#include <QObject>

class ECZIlacBilgisiYoneticisi : public QObject,
                                 public TemelVeriYoneticisi<ECZIlacBilgileri, ECZIlacBilgileriPtr>
{
    Q_OBJECT
public:
    explicit ECZIlacBilgisiYoneticisi(QObject *parent = nullptr);

    // 4. İlacın Kopyasını Oluşturulsun

    Ptr kopyaOlustur(Ptr kaynak) const;

signals:

private:
};

#endif // ECZILACBILGISIYONETICISI_H
