#include "ecztedarikciyonetici.h"
#include <Veri/VeriSiniflari/ecztedarikci.h>

ECZTedarikciYonetici::ECZTedarikciYonetici(QObject *parent) : QObject(parent)
{
    
}

ECZTedarikciYonetici::Ptr ECZTedarikciYonetici::kopyaOlustur(TemelVeriYoneticisi::Ptr kaynak) const
{
    Ptr kopya = yeni();

    kopya->setId(kaynak->getId());
    kopya->setTedarikciAdi(kaynak->getTedarikciAdi());
    kopya->setTedarikciAdresi(kaynak->getTedarikciAdresi());
    kopya->setTedarikciTelefonu(kaynak->getTedarikciTelefonu());
    kopya->setTedarikciYetkiliKisi(kaynak->getTedarikciYetkiliKisi());

    return kopya;
}
