#include "eczilacalisbilgileriyoneticisi.h"

#include <Veri/VeriSiniflari/eczilacalisbilgileri.h>

ECZIlacAlisBilgileriYoneticisi::ECZIlacAlisBilgileriYoneticisi(QObject *parent) : QObject(parent)
{
    
}

ECZIlacAlisBilgileriYoneticisi::Ptr ECZIlacAlisBilgileriYoneticisi::kopyaOlustur(
    TemelVeriYoneticisi::Ptr kaynak) const
{
    Ptr kopya = yeni();

    kopya->setId(kaynak->getId());
    kopya->setBirimFiyat(kaynak->getBirimFiyat());
    kopya->setIlacAlisMiktari(kaynak->getIlacAlisMiktari());

    return kopya;
}
