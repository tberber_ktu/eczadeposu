#ifndef TEMEL_VERI_YONETICISI_H
#define TEMEL_VERI_YONETICISI_H

#include <Veri/tanimlar.h>
#include <QObject>

template<class V, class P>
class TemelVeriYoneticisi
{
public:
    /**
     * @brief Veri Bu veri yöneticisinin yöneteceği veri türü.
     */
    typedef V Veri;
    /**
     * @brief Ptr Bu veri yöneticisinin yöneteceği veri türüne ait Pointer türü.
     */
    typedef P Ptr;
    /**
     * @brief VeriListesi Bu veri yöneticisi içinde kullanılacak veriler için liste türü.
     */
    typedef QList<Ptr> VeriListesi;
    /**
     * @brief Sart Bu veri kümesindeki filtre fonksiyonları içinde kullanılacak filtre fonksiyonu.
     */
    typedef std::function<bool(Ptr)> Sart;

    /**
     * @brief TemelVeriYoneticisi Varsayılan ilklendirici.
     * 
     * Burada sadece son id değeri sıfırlanmaktadır.
     */
    explicit TemelVeriYoneticisi() { enSonId = 0; }

    // 1. Yeni İlaç Oluşturur

    /**
     * @brief yeni Bu veri yöneticisi tarafından yönetilebilecek yeni bir nesne oluşturur.
     * 
     * @return Yeni oluşturulan nesne pointerı.
     */
    Ptr yeni() const
    {
        Ptr yeni = std::make_shared<Veri>();
        return yeni;
    }

    // 2. Listeye İlaç Ekleyeceğiz

    void ekle(Ptr ilac)
    {
        // ilacId = 0, enSonId = 0
        ilac->setId(enSonId++); // ilacId = 0, enSonId = 1
        veriler.append(ilac);
    }

    // 3. Listeden İlaç Silelim

    Ptr sil(IdTuru ilacId)
    {
        for (int i = 0; i < veriler.size(); i++) {
            if (veriler[i]->getId() == ilacId) {
                Ptr sonuc = veriler.takeAt(i);
                return sonuc;
            }
        }
        throw QObject::tr("Aranılan veri bulunamadı! Silme işlemi iptal edildi!");
    }

    Ptr sil(Ptr ilac) { return sil(ilac->getId()); }

    // 5. İlaç Aramak

    Ptr ilkiniBul(Sart f)
    {
        // I. Yol
        for (int i = 0; i < veriler.size(); i++) {
            Ptr veri_i = veriler[i];
            if (f(veri_i)) {
                return veri_i;
            }
        }

        throw QObject::tr("Aranılan veri bulunamadı!");

        // II. Yol
        // for (auto i = veriler.begin(); i != veriler.end(); i++) {
        //     Ptr veri_i = *i;
        //     if (f(veri_i)) {
        //         return veri_i;
        //     }
        // }
        //
        // throw tr("Aranılan veri bulunamadı!");
        //
        // // III. Yol
        // for (Ptr veri_i : qAsConst(veriler)) {
        //     if (f(veri_i)) {
        //         return veri_i;
        //     }
        // }
        //
        // throw tr("Aranılan veri bulunamadı!");
    }

    Ptr sonuncuyuBul(Sart f)
    {
        // I. Yol
        for (int i = veriler.size() - 1; i >= 0; i--) {
            Ptr veri_i = veriler[i];
            if (f(veri_i)) {
                return veri_i;
            }
        }

        throw QObject::tr("Aranılan veri bulunamadı!");

        // II. Yol
        // for (auto i = veriler.rbegin(); i != veriler.rend(); i++) {
        //     Ptr veri_i = *i;
        //     if (f(veri_i)) {
        //         return veri_i;
        //     }
        // }
        //
        // throw tr("Aranılan veri bulunamadı!");
    }

    VeriListesi tumunuBul(Sart f)
    {
        VeriListesi sonuc;

        for (int i = 0; i < veriler.size(); i++) {
            Ptr veri_i = veriler[i];
            if (f(veri_i)) {
                sonuc.append(veri_i);
            }
        }

        return sonuc;
    }

protected:
    VeriListesi veriler;

    IdTuru enSonId;

    template<class H, class M>
    friend QDataStream &operator<<(QDataStream &stream, TemelVeriYoneticisi<H, M> &veri);

    template<class F, class B>
    friend QDataStream &operator>>(QDataStream &stream, TemelVeriYoneticisi<F, B> &veri);
};

template<class V, class P>
QDataStream &operator<<(QDataStream &stream, TemelVeriYoneticisi<V, P> &veri)
{
    stream << veri.enSonId << veri.veriler;
    return stream;
}

template<class V, class P>
QDataStream &operator>>(QDataStream &stream, TemelVeriYoneticisi<V, P> &veri)
{
    stream >> veri.enSonId >> veri.veriler;
    return stream;
}

#endif // TEMEL_VERI_YONETICISI_H
