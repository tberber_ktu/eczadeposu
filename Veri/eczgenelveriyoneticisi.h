#ifndef ECZGENELVERIYONETICISI_H
#define ECZGENELVERIYONETICISI_H

#include <QObject>

#include <Veri/VeriYoneticileri/eczalisfaturasiyoneticisi.h>
#include <Veri/VeriYoneticileri/eczeczanebilgileriyoneticisi.h>
#include <Veri/VeriYoneticileri/eczilacalisbilgileriyoneticisi.h>
#include <Veri/VeriYoneticileri/eczilacbilgisiyoneticisi.h>
#include <Veri/VeriYoneticileri/ecztedarikciyonetici.h>

/*
 * Genel veri yöneticisi sınıfı tüm veri yöneticilerini yönetecek sınıftır.
 * Bu sınıftan sadece 1 adet üretilecektir yazılım süresince.
 * 
 * Bu sebeple bu sınıfa Singleton (Tekil) tasarım şablonu uygulanacaktır.
 * 
 */
class ECZGenelVeriYoneticisi : public QObject
{
    Q_OBJECT
private:
    // Tekil Tasarım Şablonu Adım 1: İlklendirici private yapılır.
    explicit ECZGenelVeriYoneticisi(QObject *parent = nullptr);

public:
    // Tekil Tasarım Şablonu Adım 2: static bir fonksiyon eklenir.
    /**
     * @brief sec Genel Veri Yöneticisine erişmek için kullanılacak fonksiyon.
     * 
     * Bu fonksiyon genel veri yöneticisi için oluşturulan nesneye erişim sağlar.
     * 
     * @return Genel Veri Yöneticisi nesnesi
     */
    static ECZGenelVeriYoneticisi &sec();

    // Adım 3 ve 4 cpp'de

    ECZAlisFaturasiYoneticisi &getAlisFaturalari();

    ECZEczaneBilgileriYoneticisi &getEczaneBilgileri();

    ECZIlacBilgisiYoneticisi &getIlacBilgisi();

    ECZIlacAlisBilgileriYoneticisi &getIlacAlis();

    ECZTedarikciYonetici &getTedarikci();

signals:

private:
    ECZAlisFaturasiYoneticisi alisFaturalari;
    ECZIlacAlisBilgileriYoneticisi ilacAlis;
    ECZEczaneBilgileriYoneticisi eczaneBilgileri;
    ECZIlacBilgisiYoneticisi ilacBilgisi;
    ECZTedarikciYonetici tedarikci;

    friend QDataStream &operator<<(QDataStream &a, ECZGenelVeriYoneticisi &b);
    friend QDataStream &operator>>(QDataStream &a, ECZGenelVeriYoneticisi &b);
};

QDataStream &operator<<(QDataStream &a, ECZGenelVeriYoneticisi &b);
QDataStream &operator>>(QDataStream &a, ECZGenelVeriYoneticisi &b);

#endif // ECZGENELVERIYONETICISI_H
