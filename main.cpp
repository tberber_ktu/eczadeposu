#include "eczanapencere.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ECZAnaPencere w;
    w.show();
    return a.exec();
}

// #include <Veri/eczgenelveriyoneticisi.h>
//
// #include <QDataStream>
//
// void deneme()
// {
//     auto yeniIlac = ECZGenelVeriYoneticisi::sec().getIlacBilgisi().yeni();
//
//     auto yeniEczane = ECZGenelVeriYoneticisi::sec().getEczaneBilgileri().yeni();
//
//     auto alisFaturasi = ECZGenelVeriYoneticisi::sec().getAlisFaturalari().yeni();
//
//     ECZGenelVeriYoneticisi::sec().getIlacBilgisi().ekle(yeniIlac);
//     ECZGenelVeriYoneticisi::sec().getIlacBilgisi().sil(yeniIlac);
//
//     ECZGenelVeriYoneticisi::sec().getEczaneBilgileri().ekle(yeniEczane);
//     ECZGenelVeriYoneticisi::sec().getEczaneBilgileri().sil(yeniEczane);
//
//     QDataStream dosya;
//
//     dosya << ECZGenelVeriYoneticisi::sec();
//
//     dosya >> ECZGenelVeriYoneticisi::sec();
// }
