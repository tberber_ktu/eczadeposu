#ifndef ECZALISFATURASIFORMU_H
#define ECZALISFATURASIFORMU_H

#include <QDialog>

#include <Veri/tanimlar.h>

namespace Ui {
class ECZAlisFaturasiFormu;
}

class ECZAlisFaturasiFormu : public QDialog
{
    Q_OBJECT

public:
    explicit ECZAlisFaturasiFormu(QWidget *parent = nullptr);
    ~ECZAlisFaturasiFormu();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::ECZAlisFaturasiFormu *ui;

    ECZAlisFaturasiPtr fatura;

    QList<ECZIlacAlisBilgileriPtr> ilaclar;
};

#endif // ECZALISFATURASIFORMU_H
