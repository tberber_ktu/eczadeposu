#ifndef ECZTEDARIKCIFORMU_H
#define ECZTEDARIKCIFORMU_H

#include <Veri/tanimlar.h>
#include <QDialog>

namespace Ui {
class ECZTedarikciFormu;
}

class ECZTedarikciFormu : public QDialog
{
    Q_OBJECT

public:
    explicit ECZTedarikciFormu(QWidget *parent = nullptr);
    ~ECZTedarikciFormu();

private slots:
    void on_tabTedarikciIslemleri_tabCloseRequested(int index);

    void on_btnYeniTedarikci_clicked();

    void tedarikciDuzelt(ECZTedarikciPtr tedarikci);

private:
    Ui::ECZTedarikciFormu *ui;
};

#endif // ECZTEDARIKCIFORMU_H
