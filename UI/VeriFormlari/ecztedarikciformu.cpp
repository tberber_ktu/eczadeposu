#include "ecztedarikciformu.h"
#include "ui_ecztedarikciformu.h"

#include <UI/ListeFormlari/ecztedarikcilistewidget.h>
#include <UI/VeriFormlari/Widgetlar/tedarikciduzenleme.h>

#include <Veri/VeriSiniflari/ecztedarikci.h>
#include <Veri/eczgenelveriyoneticisi.h>

ECZTedarikciFormu::ECZTedarikciFormu(QWidget *parent)
    : QDialog(parent), ui(new Ui::ECZTedarikciFormu)
{
    ui->setupUi(this);

    ECZTedarikciListeWidget *widget1 = new ECZTedarikciListeWidget(this);

    ui->tabTedarikciIslemleri->addTab(widget1, tr("Tanımlı Tedarikçiler"));

    connect(widget1,
            &ECZTedarikciListeWidget::duzeltmeTalepEdildi,
            this,
            &ECZTedarikciFormu::tedarikciDuzelt);
}

ECZTedarikciFormu::~ECZTedarikciFormu()
{
    delete ui;
}

void ECZTedarikciFormu::on_tabTedarikciIslemleri_tabCloseRequested(int index) {}

void ECZTedarikciFormu::on_btnYeniTedarikci_clicked()
{
    TedarikciDuzenleme *widget = new TedarikciDuzenleme(this);

    auto index = ui->tabTedarikciIslemleri->addTab(widget, tr("Yeni Tedarikçi Ekle"));

    auto veri = ECZGenelVeriYoneticisi::sec().getTedarikci().yeni();

    widget->setVeri(veri);

    ui->tabTedarikciIslemleri->setCurrentIndex(index);

    // Tedarikçi liste widget'ını bulalım...

    ECZTedarikciListeWidget *listeWgt = nullptr;

    for (auto i = 0; i < this->ui->tabTedarikciIslemleri->count(); i++) {
        auto ptr = this->ui->tabTedarikciIslemleri->widget(i);
        listeWgt = static_cast<ECZTedarikciListeWidget *>(ptr);
        if (listeWgt != nullptr) {
            break;
        }
    }

    connect(widget, &TedarikciDuzenleme::iptalKapat, [this, widget]() {
        for (auto i = 0; i < this->ui->tabTedarikciIslemleri->count(); i++) {
            if (this->ui->tabTedarikciIslemleri->widget(i) == widget) {
                this->ui->tabTedarikciIslemleri->removeTab(i);
                return;
            }
        }
    });

    connect(widget, &TedarikciDuzenleme::kaydetKapat, [this, widget, listeWgt]() {
        ECZGenelVeriYoneticisi::sec().getTedarikci().ekle(widget->getVeri());
        listeWgt->arama_yap();

        for (auto i = 0; i < this->ui->tabTedarikciIslemleri->count(); i++) {
            if (this->ui->tabTedarikciIslemleri->widget(i) == widget) {
                this->ui->tabTedarikciIslemleri->removeTab(i);
                return;
            }
        }
    });
}

void ECZTedarikciFormu::tedarikciDuzelt(ECZTedarikciPtr tedarikci)
{
    TedarikciDuzenleme *widget = new TedarikciDuzenleme(this);
    auto index = ui->tabTedarikciIslemleri
                     ->addTab(widget, tr("%1 Tedarikçi Düzelt").arg(tedarikci->getTedarikciAdi()));

    widget->setVeri(tedarikci);

    ui->tabTedarikciIslemleri->setCurrentIndex(index);

    // Tedarikçi liste widget'ını bulalım...

    ECZTedarikciListeWidget *listeWgt = nullptr;

    for (auto i = 0; i < this->ui->tabTedarikciIslemleri->count(); i++) {
        auto ptr = this->ui->tabTedarikciIslemleri->widget(i);
        listeWgt = static_cast<ECZTedarikciListeWidget *>(ptr);
        if (listeWgt != nullptr) {
            break;
        }
    }

    connect(widget, &TedarikciDuzenleme::iptalKapat, [this, widget]() {
        for (auto i = 0; i < this->ui->tabTedarikciIslemleri->count(); i++) {
            if (this->ui->tabTedarikciIslemleri->widget(i) == widget) {
                this->ui->tabTedarikciIslemleri->removeTab(i);
                return;
            }
        }
    });

    connect(widget, &TedarikciDuzenleme::kaydetKapat, [this, widget, listeWgt]() {
        widget->getVeri();
        listeWgt->arama_yap();

        for (auto i = 0; i < this->ui->tabTedarikciIslemleri->count(); i++) {
            if (this->ui->tabTedarikciIslemleri->widget(i) == widget) {
                this->ui->tabTedarikciIslemleri->removeTab(i);
                return;
            }
        }
    });
}
