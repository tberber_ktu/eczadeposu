#ifndef ECZYENIILACTANIMLAMAFORMU_H
#define ECZYENIILACTANIMLAMAFORMU_H

#include <Veri/tanimlar.h>
#include <QDialog>

namespace Ui {
class ECZYeniIlacTanimlamaFormu;
}

class ECZYeniIlacTanimlamaFormu : public QDialog
{
    Q_OBJECT

public:
    explicit ECZYeniIlacTanimlamaFormu(QWidget *parent = nullptr);
    ~ECZYeniIlacTanimlamaFormu();

    ECZIlacBilgileriPtr getVeri() const;
    void setVeri(const ECZIlacBilgileriPtr &value);

private:
    Ui::ECZYeniIlacTanimlamaFormu *ui;

    ECZIlacBilgileriPtr veri;
};

#endif // ECZYENIILACTANIMLAMAFORMU_H
