#include "tedarikciduzenleme.h"
#include "ui_tedarikciduzenleme.h"

#include <Veri/VeriSiniflari/ecztedarikci.h>

TedarikciDuzenleme::TedarikciDuzenleme(QWidget *parent) :
      QWidget(parent),
      ui(new Ui::TedarikciDuzenleme)
{
    ui->setupUi(this);
}

TedarikciDuzenleme::~TedarikciDuzenleme()
{
    delete ui;
}

void TedarikciDuzenleme::on_btnKaydet_clicked()
{
    emit kaydetKapat();
}

ECZTedarikciPtr TedarikciDuzenleme::getVeri() const
{
    veri->setTedarikciAdresi(ui->pteTedarikciAdresi->toPlainText());
    veri->setTedarikciAdi(ui->leTedarikciAdi->text());
    veri->setTedarikciTelefonu(ui->leTedarikciTelefon->text());
    veri->setTedarikciYetkiliKisi(ui->leTedarikciYetkiliKisi->text());

    return veri;
}

void TedarikciDuzenleme::setVeri(const ECZTedarikciPtr &value)
{
    veri = value;

    ui->leTedarikciAdi->setText(veri->getTedarikciAdi());
    ui->leTedarikciTelefon->setText(veri->getTedarikciTelefonu());
    ui->leTedarikciYetkiliKisi->setText(veri->getTedarikciYetkiliKisi());
    ui->pteTedarikciAdresi->setPlainText(veri->getTedarikciAdresi());
}

void TedarikciDuzenleme::on_btnIptal_clicked()
{
    emit iptalKapat();
}
