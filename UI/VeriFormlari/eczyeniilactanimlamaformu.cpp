#include "eczyeniilactanimlamaformu.h"
#include "ui_eczyeniilactanimlamaformu.h"

#include <Veri/VeriSiniflari/eczilacbilgileri.h>

ECZYeniIlacTanimlamaFormu::ECZYeniIlacTanimlamaFormu(QWidget *parent) :
      QDialog(parent),
      ui(new Ui::ECZYeniIlacTanimlamaFormu)
{
    ui->setupUi(this);
}

ECZYeniIlacTanimlamaFormu::~ECZYeniIlacTanimlamaFormu()
{
    delete ui;
}

ECZIlacBilgileriPtr ECZYeniIlacTanimlamaFormu::getVeri() const
{
    // I. Yol : Klasik Yöntem
    veri->setIlacAdi(ui->leIlacAdi->text());
    if (ui->rbAsi->isChecked()) {
        veri->setIlacTuru(ITAsi);
    } else if (ui->rbEfervesan->isChecked()) {
        veri->setIlacTuru(ITEfervesan);
    } else if (ui->rbFitil->isChecked()) {
        veri->setIlacTuru(ITFitil);
    } else if (ui->rbKrem->isChecked()) {
        veri->setIlacTuru(ITKrem);
    } else if (ui->rbSurup->isChecked()) {
        veri->setIlacTuru(ITSurup);
    } else {
        veri->setIlacTuru(ITTablet);
    }
    veri->setIlacEtkenMaddesi(ui->pteEtkenMadde->toPlainText());
    veri->setIlacYanEtkiler(ui->pteYanEtki->toPlainText());
    veri->setIlacEtkenMaddeMiktari(ui->dsbEtkenMaddeMiktari->value());

    // II. Yöntem
    // if (ui->rbAsi->isChecked()) {
    //     veri->setIlacTuru(ITAsi);
    // } else if (ui->rbEfervesan->isChecked()) {
    //     veri->setIlacTuru(ITEfervesan);
    // } else if (ui->rbFitil->isChecked()) {
    //     veri->setIlacTuru(ITFitil);
    // } else if (ui->rbKrem->isChecked()) {
    //     veri->setIlacTuru(ITKrem);
    // } else if (ui->rbSurup->isChecked()) {
    //     veri->setIlacTuru(ITSurup);
    // } else {
    //     veri->setIlacTuru(ITTablet);
    // }

    return veri;
}

void ECZYeniIlacTanimlamaFormu::setVeri(const ECZIlacBilgileriPtr &value)
{
    veri = value;

    // I. Yol Klasik Yöntem
    ui->leIlacAdi->setText(veri->getIlacAdi());
    // Enum için switch yazmalıyız...
    switch (veri->getIlacTuru()) {
    case ITKrem:
        ui->rbKrem->setChecked(true);
        break;
    case ITSurup:
        ui->rbSurup->setChecked(true);
        break;
    case ITAsi:
        ui->rbAsi->setChecked(true);
        break;
    case ITEfervesan:
        ui->rbEfervesan->setChecked(true);
        break;
    case ITTablet:
        ui->rbTablet->setChecked(true);
        break;
    default:
        ui->rbFitil->setChecked(true);
    }

    ui->pteEtkenMadde->document()->setPlainText(veri->getIlacEtkenMaddesi());
    ui->pteYanEtki->document()->setPlainText(veri->getIlacYanEtkiler());
    ui->dsbEtkenMaddeMiktari->setValue(veri->getIlacEtkenMaddeMiktari());

    // II. Yol Qt Yöntemi
    // ui->leIlacAdi->disconnect();
    // ui->pteEtkenMadde->disconnect();
    // ui->pteYanEtki->disconnect();
    // ui->dsbEtkenMaddeMiktari->disconnect();
    //
    // // Enum için switch yazmalıyız...
    // switch (veri->getIlacTuru()) {
    // case ITKrem:
    //     ui->rbKrem->setChecked(true);
    //     break;
    // case ITSurup:
    //     ui->rbSurup->setChecked(true);
    //     break;
    // case ITAsi:
    //     ui->rbAsi->setChecked(true);
    //     break;
    // case ITEfervesan:
    //     ui->rbEfervesan->setChecked(true);
    //     break;
    // case ITTablet:
    //     ui->rbTablet->setChecked(true);
    //     break;
    // default:
    //     ui->rbFitil->setChecked(true);
    // }
    //
    // connect(ui->leIlacAdi, &QLineEdit::textChanged, veri.get(), &ECZIlacBilgileri::setIlacAdi);
    // connect(ui->pteEtkenMadde,
    //         &QPlainTextEdit::textChanged,
    //         veri.get(),
    //         &ECZIlacBilgileri::setIlacEtkenMaddesi);
    // connect(ui->pteYanEtki,
    //         &QPlainTextEdit::textChanged,
    //         veri.get(),
    //         &ECZIlacBilgileri::setIlacYanEtkiler);
    // connect(ui->dsbEtkenMaddeMiktari,
    //         static_cast<void (QDoubleSpinBox::*)(double)> (&QDoubleSpinBox::valueChanged),
    //         veri.get(),
    //         &ECZIlacBilgileri::setIlacEtkenMaddeMiktari);
}
