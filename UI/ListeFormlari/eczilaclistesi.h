#ifndef ECZILACLISTESI_H
#define ECZILACLISTESI_H

#include <QDialog>

#include <Veri/VeriYoneticileri/eczilacbilgisiyoneticisi.h>

namespace Ui {
class ECZIlacListesi;
}

class ECZIlacListesi : public QDialog
{
    Q_OBJECT

public:
    explicit ECZIlacListesi(QWidget *parent = nullptr);
    ~ECZIlacListesi();

private slots:
    void on_btnAra_clicked();
    void ara();

private:
    void listeGuncelle();
    void arama_yap();

    Ui::ECZIlacListesi *ui;

    ECZIlacBilgisiYoneticisi::VeriListesi liste;
};

#endif // ECZILACLISTESI_H
