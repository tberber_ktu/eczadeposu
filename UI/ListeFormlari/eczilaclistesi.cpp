#include "eczilaclistesi.h"
#include "ui_eczilaclistesi.h"

#include <Veri/VeriSiniflari/eczilacbilgileri.h>
#include <Veri/eczgenelveriyoneticisi.h>

#include <QStringList>
#include <QTableWidgetItem>

#include <QPushButton>

#include <QMessageBox>

#include <UI/VeriFormlari/eczyeniilactanimlamaformu.h>

ECZIlacListesi::ECZIlacListesi(QWidget *parent) :
      QDialog(parent),
      ui(new Ui::ECZIlacListesi)
{
    ui->setupUi(this);

    arama_yap();
}

ECZIlacListesi::~ECZIlacListesi()
{
    delete ui;
}

void ECZIlacListesi::listeGuncelle()
{
    // Listedeki bilgileri ekrana aktaran fonksiyon

    // tabloyu sıfırlanır...
    ui->tblListe->clear();

    // Satır ve sütun sayısını ayarlayalım...
    ui->tblListe->setRowCount(liste.length());
    ui->tblListe->setColumnCount(8);

    // Döngü ile listeyi ekrana aktaralım...
    //Başlıklar..
    QStringList basliklar;
    basliklar << tr("İlaç ID") << tr("İlaç Adı") << tr("İlaç Türü") << tr("Yan Etkileri")
              << tr("Etken Maddesi") << tr("Etken Madde Miktarı") << tr("İlaç Sil")
              << tr("İlaç Düzelt");
    ui->tblListe->setHorizontalHeaderLabels(basliklar);

    // Veri
    for (int i = 0; i < liste.length(); i++) {
        QTableWidgetItem *hucre = new QTableWidgetItem();
        hucre->setText(tr("%1").arg(liste[i]->getId()));
        ui->tblListe->setItem(i, 0, hucre);

        hucre = new QTableWidgetItem();
        hucre->setText(liste[i]->getIlacAdi());
        ui->tblListe->setItem(i, 1, hucre);

        hucre = new QTableWidgetItem();
        switch (liste[i]->getIlacTuru()) {
        case ITKrem:
            hucre->setText("Krem");
            break;
        case ITTablet:
            hucre->setText("Tablet");
            break;
        case ITFitil:
            hucre->setText("Fitil");
            break;
        case ITEfervesan:
            hucre->setText("Efervesan");
            break;
        case ITSurup:
            hucre->setText("Şurup");
            break;
        default:
            hucre->setText("Aşı");
        }
        ui->tblListe->setItem(i, 2, hucre);

        hucre = new QTableWidgetItem();
        hucre->setText(liste[i]->getIlacYanEtkiler());
        ui->tblListe->setItem(i, 3, hucre);

        hucre = new QTableWidgetItem();
        hucre->setText(liste[i]->getIlacEtkenMaddesi());
        ui->tblListe->setItem(i, 4, hucre);

        hucre = new QTableWidgetItem();
        hucre->setText(tr("%1").arg(liste[i]->getIlacEtkenMaddeMiktari()));
        ui->tblListe->setItem(i, 5, hucre);

        QPushButton *silmeButonu = new QPushButton(this);
        silmeButonu->setText(tr("İlacı Sil"));
        ui->tblListe->setCellWidget(i, 6, silmeButonu);

        auto veri_i = liste[i];

        connect(silmeButonu, &QPushButton::clicked, [veri_i, this]() {
            auto cevap = QMessageBox::question(
                nullptr,
                tr("Silme Onayı"),
                tr("%1 isimli ilacı simek istediğinize emin misiniz?").arg(veri_i->getIlacAdi()));
            if (cevap == QMessageBox::Yes) {
                ECZGenelVeriYoneticisi::sec().getIlacBilgisi().sil(veri_i->getId());
                QMessageBox::information(nullptr,
                                         tr("Kayıt Silindi"),
                                         tr("Kayıt Silme işlemi tamamlandı!"));
                this->arama_yap();
            }
        });

        QPushButton *duzeltmeButonu = new QPushButton(this);
        duzeltmeButonu->setText(tr("İlacı Düzelt"));
        ui->tblListe->setCellWidget(i, 7, duzeltmeButonu);

        connect(duzeltmeButonu, &QPushButton::clicked, [veri_i, this]() {
            ECZYeniIlacTanimlamaFormu form;

            form.setVeri(veri_i);

            form.setWindowTitle(tr("%1 İlacını Düzenle").arg(veri_i->getIlacAdi()));

            if (form.exec() == QDialog::Accepted) {
                form.getVeri();
                this->listeGuncelle();
            }
        });
    }
}

void ECZIlacListesi::arama_yap()
{
    // Seçilen filtreye göre liste oluşturacak

    // İsimsiz Fonksiyon (Lambda Function)

    // int a = 5;

    /**
     * double kare(double b) {
     *    return b*b;
     * }
     * 
     * [ ] (double b) -> double {
     *    return b*b;
     * }
     * 
     */

    // std::function<double(double)> b = [a](double x) -> double {
    //     if (a < 10) {
    //         return x * x;
    //     } else {
    //         return x * x * x;
    //     }
    // };
    //
    // double c = b(5);

    auto ekran = this->ui;

    liste = ECZGenelVeriYoneticisi::sec().getIlacBilgisi().tumunuBul(
        [ekran](ECZIlacBilgisiYoneticisi::Ptr veri) -> bool {
            if (ekran->leIlacAdiDegeri->text() == "" && ekran->leEtkenMaddeDegeri->text() == "") {
                return true;
            }

            if (ekran->leIlacAdiDegeri->text() != "") {
                if (ekran->rbIlacAdiIleBaslayan->isChecked()) {
                    if (!veri->getIlacAdi().toLower().startsWith(
                            ekran->leIlacAdiDegeri->text().toLower())) {
                        return false;
                    }
                } else if (ekran->rbIlacAdiIleBiten->isChecked()) {
                    if (!veri->getIlacAdi().toLower().endsWith(
                            ekran->leIlacAdiDegeri->text().toLower())) {
                        return false;
                    }
                } else if (ekran->rbIlacAdiIceren->isChecked()) {
                    if (!veri->getIlacAdi().toLower().contains(
                            ekran->leIlacAdiDegeri->text().toLower())) {
                        return false;
                    }
                }
            }

            if (ekran->leEtkenMaddeDegeri->text() != "") {
                if (ekran->rbEtkenMaddeIleBaslayan->isChecked()) {
                    if (!veri->getIlacEtkenMaddesi().toLower().startsWith(
                            ekran->leEtkenMaddeDegeri->text().toLower())) {
                        return false;
                    }
                } else if (ekran->rbEtkenMaddeIleBiten->isChecked()) {
                    if (!veri->getIlacEtkenMaddesi().toLower().endsWith(
                            ekran->leEtkenMaddeDegeri->text().toLower())) {
                        return false;
                    }
                } else if (ekran->rbEtkenMaddeIceren->isChecked()) {
                    if (!veri->getIlacEtkenMaddesi().toLower().contains(
                            ekran->leEtkenMaddeDegeri->text().toLower())) {
                        return false;
                    }
                }
            }

            return true;
        });

    listeGuncelle();
}

void ECZIlacListesi::ara()
{
    if (ui->cbYazarkenAra->isChecked()) {
        arama_yap();
    }
}

void ECZIlacListesi::on_btnAra_clicked()
{
    arama_yap();
}
