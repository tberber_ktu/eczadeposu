#ifndef ECZTEDARIKCILISTEWIDGET_H
#define ECZTEDARIKCILISTEWIDGET_H

#include <Veri/VeriYoneticileri/ecztedarikciyonetici.h>
#include <QWidget>

namespace Ui {
class ECZTedarikciListeWidget;
}

class ECZTedarikciListeWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ECZTedarikciListeWidget(QWidget *parent = nullptr);
    ~ECZTedarikciListeWidget();

    void arama_yap();

signals:
    void duzeltmeTalepEdildi(ECZTedarikciYonetici::Ptr tedarikci);

private slots:
    void ara();

private:
    void listeGuncelle();

    Ui::ECZTedarikciListeWidget *ui;

    ECZTedarikciYonetici::VeriListesi liste;
};

#endif // ECZTEDARIKCILISTEWIDGET_H
