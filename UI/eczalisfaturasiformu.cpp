#include "eczalisfaturasiformu.h"
#include "ui_eczalisfaturasiformu.h"

#include <Veri/VeriSiniflari/eczalisfaturasi.h>
#include <Veri/VeriSiniflari/eczilacalisbilgileri.h>
#include <Veri/VeriSiniflari/eczilacbilgileri.h>
#include <Veri/eczgenelveriyoneticisi.h>

ECZAlisFaturasiFormu::ECZAlisFaturasiFormu(QWidget *parent) :
      QDialog(parent),
      ui(new Ui::ECZAlisFaturasiFormu)
{
    ui->setupUi(this);
}

ECZAlisFaturasiFormu::~ECZAlisFaturasiFormu()
{
    delete ui;
}

void ECZAlisFaturasiFormu::on_pushButton_2_clicked()
{
    auto ilacAlis = ECZGenelVeriYoneticisi::sec().getIlacAlis().yeni();

    ilacAlis->setIlacId(1);
    ilacAlis->setIlacAlisMiktari(100);
    ilacAlis->setBirimFiyat(100);

    ilaclar.append(ilacAlis);

    // Tablo güncelle
}

void ECZAlisFaturasiFormu::on_pushButton_clicked()
{
    auto fatura = ECZGenelVeriYoneticisi::sec().getAlisFaturalari().yeni();

    fatura->setTedarikci(1);
    fatura->setFaturaTarihi(QDateTime::currentDateTime());
    fatura->setFaturaNo("A1000");

    ECZGenelVeriYoneticisi::sec().getAlisFaturalari().ekle(fatura);

    for (auto alinanIlac : ilaclar) {
        alinanIlac->setAlisFaturaId(fatura->getId());
        ECZGenelVeriYoneticisi::sec().getIlacAlis().ekle(alinanIlac);
    }

    // Başka bir yerde....
    int i = 5;

    fatura->getAlinanIlaclar()[i]->getFatura()->getAlinanIlaclar()[i + 1]->getIlac()->getIlacAdi();
}
