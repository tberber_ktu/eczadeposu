#include "eczanapencere.h"
#include "ui_eczanapencere.h"

#include <QDataStream>
#include <QDir>
#include <QFile>
#include <QStandardPaths>

#include <Veri/eczgenelveriyoneticisi.h>

#include <UI/VeriFormlari/eczyeniilactanimlamaformu.h>

#include <UI/ListeFormlari/eczilaclistesi.h>

#include <UI/VeriFormlari/ecztedarikciformu.h>

#include <Veri/VeriSiniflari/eczilacbilgileri.h>

#include <Veri/VeriSiniflari/eczalisfaturasi.h>

#include <Veri/VeriSiniflari/ecztedarikci.h>

#include <QDebug>

ECZAnaPencere::ECZAnaPencere(QWidget *parent)
    : QMainWindow(parent)
      , ui(new Ui::ECZAnaPencere)
{
    ui->setupUi(this);

    // Verilerimizi dosyadan okuyalım
    QString dosya_yolu = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    QDir dosya_klasoru(dosya_yolu);

    if (dosya_klasoru.exists("eczane.db")) {
        QFile dosya(dosya_yolu + "/eczane.db");

        if (dosya.open(QIODevice::ReadOnly)) {
            QDataStream oku(&dosya);

            oku >> ECZGenelVeriYoneticisi::sec();

            dosya.close();
        }
    }

    auto tumIlaclar = ECZGenelVeriYoneticisi::sec().getIlacBilgisi().tumunuBul(
        [](ECZIlacBilgileriPtr) { return true; });

    for (auto ilac : tumIlaclar) {
        ui->comboBox
            ->addItem(tr("%1 (E.M.:%2)").arg(ilac->getIlacAdi()).arg(ilac->getIlacEtkenMaddesi()),
                      ilac->getId());
    }
}

ECZAnaPencere::~ECZAnaPencere()
{
    delete ui;

    // Verilerimizi dosyadan okuyalım
    QString dosya_yolu = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);

    QFile dosya(dosya_yolu + "/eczane.db");
    if (dosya.open(QIODevice::WriteOnly)) {
        QDataStream yaz(&dosya);

        yaz << ECZGenelVeriYoneticisi::sec();

        dosya.close();
    }
}

void ECZAnaPencere::on_actionYeni_la_Tan_mlama_triggered()
{
    ECZYeniIlacTanimlamaFormu form;
    auto veri = ECZGenelVeriYoneticisi::sec().getIlacBilgisi().yeni();

    form.setVeri(veri);
    form.setWindowTitle(tr("Yeni İlaç Ekleme"));

    if (form.exec() == QDialog::Accepted) {
        form.getVeri();
        ECZGenelVeriYoneticisi::sec().getIlacBilgisi().ekle(veri);
    }
}

void ECZAnaPencere::on_actionMevcut_la_lar_triggered()
{
    ECZIlacListesi form;

    form.exec();
}

void ECZAnaPencere::on_actionYeni_Tedarik_i_Tan_mla_triggered()
{
    ECZTedarikciFormu form;

    form.exec();
}

void ECZAnaPencere::on_pushButton_clicked()
{
    IdTuru id = ui->comboBox->currentData().toInt();
    qDebug() << id;
    auto metin = ui->comboBox->currentText();
    qDebug() << metin;
    auto index = ui->comboBox->currentIndex();
    qDebug() << index;
}

void ECZAnaPencere::on_label_linkActivated(const QString &link)
{
    if (link == "#ekle") {
        on_actionYeni_la_Tan_mlama_triggered();
    } else if (link == "#duzelt") {
    }
}
