#ifndef ECZANAPENCERE_H
#define ECZANAPENCERE_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class ECZAnaPencere; }
QT_END_NAMESPACE

class ECZAnaPencere : public QMainWindow
{
    Q_OBJECT

public:
    ECZAnaPencere(QWidget *parent = nullptr);
    ~ECZAnaPencere();

private slots:
    void on_actionYeni_la_Tan_mlama_triggered();

    void on_actionMevcut_la_lar_triggered();

    void on_actionYeni_Tedarik_i_Tan_mla_triggered();

    void on_pushButton_clicked();

    void on_label_linkActivated(const QString &link);

private:
    Ui::ECZAnaPencere *ui;
};
#endif // ECZANAPENCERE_H
